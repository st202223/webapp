const http = require('http');
const path = require('path');
const fs = require('fs');

const PORT = 3000;

http.createServer( (req, res)=>{

    const potDoDatoteke = path.join(__dirname, 'public', (req.url==="/")? "index.html" : req.url);
    let contentType = "text/html";
    switch (path.extname(req.url)) {
        case ".jpg":
                contentType = "image/jpg";
            break;
        case ".png":
                contentType = "image/png";
            break;
        case ".css":
                contentType = "text/css";
            break;
    
        default:
            break;
    }

    fs.readFile(potDoDatoteke, (err, data)=>{
        if (err){
            if (err.code === "ENOENT"){
                fs.readFile(path.join(__dirname, 'public', '404.html'), (err2, data2)=>{
                    res.writeHead(200, {'Content-type':'text/html'});
                    res.end(data2);
                })
            } else {
                res.writeHead(200, {'Content-type':'application/jsopn'});
                res.end(JSON.stringify({'Error':err.message}));
            }
        } else {
            res.writeHead(200, {'Content-type':contentType});
            res.end(data);
        }
    });

    if (req.url==="/api/students"){
        res.writeHead(200, {'Content-type':'application/json'});
        const students = [
            {ime: "Janez", priimek: "Novak", vpisnaSt:"101010102", email: "janez.novak@um.si"},
            {ime: "Mirko", priimek: "Krajnc", vpisnaSt:"4545653222", email: "mirko.krajnc@um.si"},
            {ime: "Andrej", priimek: "Prevc", vpisnaSt:"435433534543", email: "andrej.prevc@um.si"},
        ];
        res.end(JSON.stringify(students));
    }


}).listen(PORT, ()=>{
    console.log('Strežnik posluša na http://localhost:' + 3000);
})